package example.micronaut

import io.micronaut.context.ApplicationContext
import io.micronaut.http.client.HttpClient
import io.micronaut.runtime.server.EmbeddedServer
import org.amshove.kluent.shouldEqual
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

object HelloControllerSpec : Spek({
    describe("HelloController Suite") {
        val embeddedServer: EmbeddedServer = ApplicationContext.run(EmbeddedServer::class.java)
        val client: HttpClient = HttpClient.create(embeddedServer.url)

        it("test /hello responds [Hello World]") {
            val rsp: List<String> = client.toBlocking().retrieve("/hello", List::class.java as Class<List<String>>)

            rsp shouldEqual listOf("Hello World")
        }

        afterGroup {
            client.close()
            embeddedServer.close()
        }
    }
})
